Vapour
========================

This repository contains the vapour application code.

* [Installing](http://bitbucket.com/studduck/vapour/overview#markdown-header-installing)
* [Usage](http://bitbucket.com/studduck/vapour/overview#markdown-header-usage)
* [Building config files](http://bitbucket.com/studduck/vapour/overview#markdown-header-building-config-values)
* [Base configuration](http://bitbucket.com/studduck/vapour/overview#markdown-header-base-configuration)
* [Configuration values](http://bitbucket.com/studduck/vapour/overview#markdown-header-configuration-values)
* [Resource examples](http://bitbucket.com/studduck/vapour/overview#markdown-header-resource-examples)
* [Extending vapour resources](http://bitbucket.com/studduck/vapour/overview#markdown-header-extending-vapour-resources)
* [Running tests](http://bitbucket.com/studduck/vapour/overview#markdown-header-running-tests)

  

## Why?

We found after building multiple cloud formation stacks we were duplicating alot of configuration values. Things like a VPC ids and subnet ids were the same in every stack across an environment.

Unfortunatly there is no way to add common parameters across multiple cloud formation stacks. Vapour was built to allow inheritence for cloud formation resources. We define common values in a base configuration file and then merge these with a stack file. The values in the stack file overwrite the base values and keep any that are required.

You can also define multiple environments in one config file. This makes it easy to build common stacks in multiple environments.


## Installing

----------------------- 

After checking out this repository you will need to install Python 3 and the Python 3 dev tools.
You will also need to install the xml and [yaml](http://pyyaml.org/) libraries for python

If you are running a debian based system you can run the bootstrap.sh file and it will install it for you.  
```
./boostrap.sh
```

Once you have python installed install the required pip packages   
```
sudo pip3 install -r requirements.txt
```

  
  
## Usage

----------------------- 

Vapour reads yaml files and converts them into json templates using [troposphere](https://github.com/cloudtools/troposphere).


To build a template run vapour by passing in config file and output directory  
```
python3 ./vapour vapour/examples templates
```

Vapour will read the config file, merge it with a base file if it exists and then output a cloud formation template.
  
  

## Building config files

----------------------- 

The YAML file has four required sections.

__name__: The name of the cloud formation stack  
__description__: The description of the cloud formation stack  
__environment__: The applications environment (dev, test, uat, prod, etc...)  
__resources__: A list of aws resources and their configuration values  

The environment attribute can be a list, vapour will build one template for each environment defined.

```yaml
name: Example cloud formation stack  
description: Builds the infrastructure for an example cloud formation stack.  
environment: ['dev', 'uat', 'prod']  
resources:  
    rds: ...  
```
  
The resources will be combined with any values in the base configuration for that environment.  
If you have more than one resource of the same type you will need to use the following naming convention.

\#{name}__#{type}: 

```yaml
name: Example cloud formation stack  
...
resources:  
    cms__security_group: ...
    fe__security_group: ...
```

Both the security group resources above will have acess to any defaults in the base configruration file.

  
## Base configuration

----------------------- 

It is possible to set default configuration values that are consistent across stacks for an environment eg. VPC id, Subnet ids etc.

To do this you can create a base configuration file with the following naming convention:
__base_#{environment}__.yml

```yaml
#__base_dev__.yml

resources:
    security_group:
        vpcId: vpc-57a77732
        port: 80
        protocol: "TCP"
```

Any configuration files that use the dev environment will automatically have the above security group defaults. 


## Configuration values

-----------------------

### Env
You can create references to the current environment by adding Env__ to a value.

environment: ['dev', 'uat', 'prod']

eg.

...
bucketName: Env__-bucket

This config entry will add the environment to each template

...
"BucketName": "dev-bucket",
...

### Ref
You can also create reference objects by prepending Ref__ to the value.

eg.
customSecurityGroupIds: ["Ref__InstanceRDSSecurityGroup"]

This config entry will output the following value:

"CustomSecurityGroupIds": [
    {
        "Ref": "InstanceRDSSecurityGroup"
    }
]

### Fn::GetAtt
You can get attributes from other objects in your stack by prepending Att__ and appending :key to the value

eg.
customSecurityGroupIds: ["Att__fapiSG:GroupId"]

This config entry will output the following value:

"CustomSecurityGroupIds": [                    
    {
        "Fn::GetAtt": [
            "fapiSG",
            "GroupId"
        ]
    }
]
  

## Resource examples
----------------------- 

Currently supported aws types:

* Auto Scaling Group
* Cloud Watch Alarm
* EC2
* IAM
* Launch Configuration
* Load Balancer
* [OpsWorks](http://bitbucket.com/studduck/vapour/overview/vapour/examples/opsworks-example.yml)
* Parameter
* [RDS](http://bitbucket.com/studduck/vapour/overview/vapour/examples/rds-example.yml)
* Record Set
* S3
* S3 Bucket Policy
* [Security group](http://bitbucket.com/studduck/vapour/overview/vapour/examples/security-groups-example.yml)
* Security Group Ingress
* SQS Queue

You can run vapour over the examples directory to see the example cloud formation output

```
python3 ./vapour examples templates
```

  
Please note that the example files contain alot of common values that could be placed in a base configuration file.


## Extending vapour resources
----------------------- 

To add a new resource you will need to add a module to vapour.
When vapour reads a resource type in the yaml configuration it automatically passes the stack name, environment, template and config to the add method in the corresponding python module.

The resource would need to also exist in troposphere](https://github.com/cloudtools/troposphere).

Example:  

```yaml
Name: test stack
Environment: dev
Resources:
    new_resource:
        config: value
        config2: value2
```

```python
# new_resource.py
def add(stack_name, environment, template, config)
 
stack_name: The name configured in the yaml file (test stack)
environment: The environment configured in the yaml file (dev)
template: The Troposhere Template object
config: The configuration (config: value, config2: value2)
```

Your module should take the configuration and add the [Troposphere](https://github.com/cloudtools/troposphere) resource type to the template object.

It will then be output in the json template


## Running tests

To run the tests you need to add vapour to your python path

```
export PYTHONPATH=/var/projects/vapour
```

Then run the tests

```
nosetests --with-xunit --xunit-file=reports/junit.xml 
----------------------------------------------------------------------
Ran 8 tests in 0.019s

```
#!/usr/bin/env python3
"""This module builds JSON for NLMs cloudformation stacks from yaml file."""
import sys
import os
import template
from files import FileHandler
from config import ConfigFactory
from resource import ResourceHandler
from troposphere import Template

def bootstrap():
    """Take user input and bootstrap vapour"""

    if len(sys.argv) > 1:
        file_location = sys.argv[1]
    else:
        file_location = input(
            'Please pass a path containing your yaml file or files: '
            )

    if len(sys.argv) > 2:
        output_directory_name = sys.argv[2]
    else:
        output_directory_name = input(
            'Please pass the output directory for the cloudformation scripts: '
            )

    file_location = os.path.abspath(file_location)
    output_directory_name = os.path.abspath(output_directory_name)

    factory = ConfigFactory()
    stack_configs = factory.load_config_file_from_path(file_location)

    for name, config_list in stack_configs.items():

        for config in config_list:
            template.build(
                Template(),
                name,
                config,
                output_directory_name,
                FileHandler(),
                ResourceHandler(),
                factory
            )

bootstrap()

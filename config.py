"""Contains the config values used to build the cloudformation templates"""
import yaml, os, copy, re
from troposphere import Ref, GetAtt

class Config:
    """Represents the config values for a stack"""
    def __init__(self, name, values, environment):
        self.name = name
        self.values = values
        self.environment = environment

    def get(self, key=None, required=True):
        """Return this configs values"""

        if self.__validate(key, required):
            return self.get_parameter(self.values[key])

    def get_all(self, key=None):
        """Return all config values. Can filter by key"""
        if key == None:
            return self.values
        else:
            values = []
            for value_key in self.values.keys():
                if value_key == key or Config.get_module_key_from_resource(value_key) == key:
                    values.append(Config(value_key, self.values[value_key], self.environment))
            return values

    def __validate(self, value, required):
        """Validates the config values to ensure the expected values exist"""

        if not value in self.values.keys():
            if required:
                raise ValueError(value + ' does not exist in ' + self.name + ' values')
            else:
                return False
        return True

    def get_parameter(self, value):
        """
            Returns the troposphere parameter for this value.
            If the value is a prepended with __Ref it will
            return a Ref object. If the value is prepened with Att__
            it will return a GetAtt object.
        """
        if isinstance(value, str):
            if 'Ref__' in value:
                value = value.split('__', 1)[-1]
                return Ref(value)
            elif 'Env__' in value:
                return value.replace('Env__', self.environment)
            elif 'Att__' in value and ':' in value:
                att_data = value.split('__', 1)[-1].split(':', 1)
                return GetAtt(att_data[0], att_data[-1])
        elif isinstance(value, list):
            new_value = []
            for list_item in value:
                new_value.append(self.get_parameter(list_item))

            return new_value
        elif isinstance(value, dict):
            new_value = {}
            for key in value:
                new_value[key] = self.get_parameter(value[key])

            return new_value

        return value

    @staticmethod
    def get_module_key_from_resource(key):
        """
            Strips the resource string delimiter and
            returns the module type
        """
        return key.split('__', 1)[-1]

    @staticmethod
    def get_resource_name(value):
        """
        Strips out characters that
        cant be used in resource names
        """
        return re.sub('[^0-9a-zA-Z]+', '', value)

class ConfigFactory:
    """Builds Config classes from values or files"""
    __BASE_FILE_NAME = '__base_'

    def build(self, name, values, environment):
        return Config(name, values, environment)

    def load_config_file_from_path(self, file_path):
        """Extracts the root and file names of any config files"""
        if os.path.isdir(file_path):
            for root, sub_folders, config_files in os.walk(file_path):
                files = config_files
        else:
            files = [os.path.basename(file_path)]
            root = os.path.dirname(file_path)

        return self.create_config_files(root, files)

    def create_config_files(self, root, config_files):
        """Load the stack config files and combines them with the base"""
        stack_configs = {}
        for stack_file in config_files:

            name = stack_file.replace('.yml', '')
            stack_configs[name] = []

            if not self.__BASE_FILE_NAME in stack_file:
                with open(os.path.join(root, stack_file), 'r') as fin:
                    raw_file_content = yaml.load(fin)

                    if 'environment' not in raw_file_content:
                        import sys
                        print('environment not set in ' + stack_file)
                        sys.exit(0)
                    else:
                        environments = raw_file_content['environment']

                    if not isinstance(environments, list):
                        environments = [environments]

                    for environment in environments:

                        file_content = copy.deepcopy(raw_file_content)

                        file_content['environment'] = environment

                        base_file_name = self.__BASE_FILE_NAME + \
                            environment + \
                            '__.yml'

                        try:
                            with open(os.path.join(root, base_file_name), 'r') as fin:
                                base_config = yaml.load(fin)

                                resources = file_content['resources']

                                for resource_key, stack_resource in resources.items():

                                    module_key = Config.get_module_key_from_resource(resource_key)

                                    if module_key in base_config['resources']:
                                        file_content['resources'][resource_key] = \
                                        self.__merge_config_files(
                                            stack_resource,
                                            base_config['resources'][module_key]
                                        )
                        except FileNotFoundError:
                            import sys
                            print(
                                base_file_name +
                                " was not found in "+
                                root +
                                ". Ignoring..."
                            )

                        stack_configs[name].append(self.build(name, file_content, environment))

        return stack_configs

    @staticmethod
    def __merge_config_files(stack_config, base_config):
        """
            Overwrite base config with the stack file
            Allows for multiple resources of the same
            type by splitting keys using __ see
            get_module_key_from_resource definition
        """

        new_stack_config = copy.deepcopy(stack_config)

        for base_key in base_config:

            # Match any config values including any with prepended string (ie. string__resource)
            # Rules:
            # If value in base config but not in stack config and it's not a dictonary add it
            # If value is in stack config and is a dictionary recurse over the sub values
            # If value is in stack config and is a list merge the two lists
            # A module can be defined using the naming convention name__type where type
            # is the resource type and name is an identifier
            for stack_key in stack_config:
                matched_key = None
                if stack_key == base_key or \
                Config.get_module_key_from_resource(stack_key) == base_key:
                    matched_key = stack_key

                if matched_key is None and \
                not isinstance(base_config[base_key], dict) and \
                base_key not in new_stack_config:
                    new_stack_config[base_key] = base_config[base_key]

                elif matched_key is not None:
                    if isinstance(base_config[base_key], dict):
                        new_stack_config[matched_key] = \
                            ConfigFactory.__merge_config_files(stack_config[matched_key], base_config[base_key])

                    elif isinstance(base_config[base_key], list):
                        new_stack_config[matched_key] = \
                        base_config[base_key] + stack_config[matched_key]

            # add any missing items that were in base_config but not the stack_config
            if base_key not in new_stack_config:
                # make sure the item isn't a module
                is_module = False
                for stack_key in stack_config:
                    if '__'+base_key in stack_key:
                        is_module = True
                        break

                if not is_module:
                    base_dict = { base_key: base_config[base_key] }
                    new_stack_config = dict(base_dict, **new_stack_config)

        return new_stack_config

"""File handling module"""
import os

class FileHandler:
    """Handles file creation"""

    def write(self, file_name, directory, content):
        """Writes the template content to a file"""
        if not os.path.exists(directory):
            os.makedirs(directory, 0o754)

        output_file = open(file_name, 'w')
        output_file.write(content)
        output_file.close()
        print('\n' + file_name + ' created successfully')



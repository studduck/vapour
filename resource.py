"""The resource handler"""
import importlib

class ResourceHandler:
    """Handles resource creation"""
    __RESOURCE_DIR = 'resources'

    def add(self, resource_name, environment, template, config):
        """Adds the specified resource to the template"""
        try:
            resource_mod = importlib.import_module(self.__RESOURCE_DIR + '.' + resource_name)
        except ImportError as error:
            import sys
            raise type(error)(
                str(error) +
                '. The resource types module could not be automatically imported.' +
                ' Please check that vapour has a module named: ' +
                resource_name
                ).with_traceback(sys.exc_info()[2])
        resource_mod.add(
            resource_name.replace('_', '-'),
            environment,
            template,
            config
        )

def add_value(resource, name, value):
    """Adds a value to a resource if the value is not empty"""
    if value != None:
        resource.__setattr__(name, value)

"""Create Auto Scaling Groups"""
from troposphere.autoscaling import AutoScalingGroup
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add Launch configurations to a given template
    """

    auto_scaling_group = AutoScalingGroup(
        Config.get_resource_name(config.get('name')),
        MaxSize=config.get('maxSize'),
        MinSize=config.get('minSize')
    )

    add_value(auto_scaling_group, 'AvailabilityZones', config.get('availabilityZones', False))
    add_value(auto_scaling_group, 'Cooldown', config.get('cooldown', False))
    add_value(auto_scaling_group, 'DesiredCapacity', config.get('desiredCapacity', False))
    add_value(
        auto_scaling_group,
        'HealthCheckGracePeriod',
        config.get('healthCheckGracePeriod', False)
    )
    add_value(auto_scaling_group, 'HealthCheckType', config.get('healthCheckType', False))
    add_value(auto_scaling_group, 'InstanceId', config.get('instanceId', False))
    add_value(
        auto_scaling_group,
        'LaunchConfigurationName',
        config.get('launchConfigurationName', False)
    )
    add_value(auto_scaling_group, 'LoadBalancerNames', config.get('loadBalancerNames', False))
    add_value(auto_scaling_group, 'MetricsCollection', config.get('metricsCollection', False))
    add_value(auto_scaling_group, 'Tags', config.get('tags', False))
    add_value(auto_scaling_group, 'VPCZoneIdentifier', config.get('vpcZoneIdentifier', False))

    template.add_resource(auto_scaling_group)

"""Create Cloud Watch Alarms"""
from troposphere.cloudwatch import Alarm, MetricDimension
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add cloud watch alarms to the given template
    """
    name = config.get('alarmName')

    alarm = Alarm(
        Config.get_resource_name(name),
        AlarmName=name,
        ComparisonOperator=config.get('comparisonOperator'),
        EvaluationPeriods=config.get('evaluationPeriods'),
        MetricName=config.get('metricName'),
        Namespace=config.get('namespace'),
        Period=config.get('period'),
        Statistic=config.get('statistic'),
        Threshold=config.get('threshold')
    )

    add_value(alarm, 'ActionsEnabled', config.get('actionsEnabled', False))
    add_value(alarm, 'AlarmActions', config.get('alarmActions', False))
    add_value(alarm, 'AlarmDescription', config.get('alarmDescription', False))


    dimensions_config = config.get('dimensions', False)

    if dimensions_config:

        dimensions = []

        for dimension in dimensions_config:
            dimension = Config('dimension', dimension, environment)
            dimensions.append(
                MetricDimension(
                    Name=dimension.get('Name'),
                    Value=dimension.get('Value')
                )
            )

        add_value(alarm, 'Dimensions', dimensions)

    add_value(alarm, 'InsufficientDataActions', config.get('insufficientDataActinos', False))
    add_value(alarm, 'OKActions', config.get('okActions', False))
    add_value(alarm, 'Unit', config.get('unit', False))

    template.add_resource(alarm)

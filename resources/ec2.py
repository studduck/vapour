"""Create ec2 instances"""
from troposphere.ec2 import Instance, NetworkInterfaceProperty, PrivateIpAddressSpecification
from troposphere.ec2 import SsmAssociations, AssociationParameters, MountPoint
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @stack_name The name of the stack
    @environment The environment (dev,uat,prod)
    @template Template
    @config: Config
    Add ec2 instance to the given template
    """

    instance = Instance(
        config.get_resource_name(config.get('name')),
        ImageId=config.get('imageId')
    )

    add_value(instance, 'AvailabilityZone', config.get('availabilityZone', False))
    add_value(instance, 'BlockDeviceMappings', config.get('blockDeviceMappings', False))
    add_value(instance, 'DisableApiTermination', config.get('disableApiTermination', False))
    add_value(instance, 'EbsOptimized', config.get('ebsOptimized', False))
    add_value(instance, 'IamInstanceProfile', config.get('iamInstanceProfile', False))
    add_value(
        instance,
        'InstanceInitiatedShutdownBehavior',
        config.get('instanceInitiatedShutdownBehavior', False)
    )
    add_value(instance, 'InstanceType', config.get('instanceType', False))
    add_value(instance, 'KernelId', config.get('kernelId', False))
    add_value(instance, 'KeyName', config.get('keyName', False))
    add_value(instance, 'Monitoring', config.get('monitoring', False))

    network_configs = config.get('networkInterfaces', False)

    if network_configs:

        network_interfaces = []

        for network_config in network_configs:

            network_config = Config('network', network_config, environment)

            network_interface = NetworkInterfaceProperty(
                DeviceIndex=network_config.get('deviceIndex')
            )

            add_value(
                network_interface,
                'AssociatePublicIpAddress',
                network_config.get('associatePublicIpAddress', False)
            )
            add_value(
                network_interface,
                'DeleteOnTermination',
                network_config.get('deleteOnTermination', False)
            )
            add_value(network_interface, 'Description', network_config.get('description', False))
            add_value(network_interface, 'DeviceIndex', network_config.get('deviceIndex', False))
            add_value(network_interface, 'GroupSet', network_config.get('groupSet', False))
            add_value(
                network_interface,
                'NetworkInterfaceId',
                network_config.get('networkInterfaceId', False)
            )
            add_value(
                network_interface,
                'PrivateIpAddress',
                network_config.get('privateIpAddress', False)
            )
            add_value(
                network_interface,
                'SecondaryPrivateIpAddressCount',
                network_config.get('secondaryPrivateIpAddressCount', False)
            )
            add_value(network_interface, 'SubnetId', network_config.get('subnetId', False))


            private_ip_address_configs = network_config.get('privateIpAddresses', False)

            if private_ip_address_configs:

                private_ip_addresses = []

                for private_ip_address_config in private_ip_address_configs:

                    private_ip_address_config = Config('privateIp', private_ip_address_config, environment)

                    private_ip_addresses.append(
                        PrivateIpAddressSpecification(
                            Primary=private_ip_address_config.get('primary'),
                            PrivateIpAddress=private_ip_address_config.get('privateIpAddress')
                        )
                    )

                add_value(network_interface, 'PrivateIpAddresses', private_ip_addresses)

            network_interfaces.append(network_interface)

        add_value(instance, 'NetworkInterfaces', network_interfaces)

    add_value(instance, 'PlacementGroupName', config.get('placementGroupName', False))
    add_value(instance, 'PrivateIpAddress', config.get('privateIpAddress', False))
    add_value(instance, 'RamdiskId', config.get('ramdiskId', False))
    add_value(instance, 'SecurityGroupIds', config.get('securityGroupIds', False))
    add_value(instance, 'SecurityGroups', config.get('securityGroups', False))
    add_value(instance, 'SourceDestCheck', config.get('sourceDestCheck', False))

    ssm_associations_config = config.get('ssmAssociations', False)

    if ssm_associations_config:

        ssm_associations = []

        for ssm_association_config in ssm_associations_config:

            ssm_association_config = Config('ssm', ssm_association_config, environment)

            ssm_association = SsmAssociations(
                DocumentName=ssm_association_config.get('documentName')
            )

            association_parameters_config = ssm_association_config.get(
                'associationParameters',
                False
            )

            if association_parameters_config:

                association_parameters = []

                for association_parameter_config in association_parameters_config:

                    association_parameter_config = Config(
                        'association',
                        association_parameter_config,
                        environment
                    )

                    association_parameters.append(
                        AssociationParameters(
                            Key=association_parameter_config.get('key'),
                            Value=association_parameter_config.get('value')
                        )
                    )

                add_value(ssm_association, 'AssociationParameters', association_parameters)

            ssm_associations.append(ssm_association)

        add_value(instance, 'SsmAssociations', ssm_associations)

    add_value(instance, 'SubnetId', config.get('subnetId', False))
    add_value(instance, 'Tags', config.get('tags', False))
    add_value(instance, 'Tenancy', config.get('tenancy', False))
    add_value(instance, 'UserData', config.get('userData', False))

    volumes_config = config.get('volumes', False)

    if volumes_config:
        mount_points = []

        for volume_config in volumes_config:

            volume_config = Config('volume', volume_config, environment)

            mount_points.append(
                MountPoint(
                    Device=volume_config.get('device'),
                    VolumeId=volume_config.get('volumeId')
                )
            )

        add_value(instance, 'Volumes', mount_points)

    template.add_resource(instance)


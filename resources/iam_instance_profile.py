"""Create IAM Roles"""
from troposphere.iam import Role, Policy, InstanceProfile, Ref
from config import Config

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add IAM roles to the given template
    """

    template.add_resource(
        InstanceProfile(
            Config.get_resource_name(config.get('name')),
            Path=config.get('path'),
            Roles=config.get('roles')
        )
    )

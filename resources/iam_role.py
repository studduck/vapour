"""Create IAM Roles"""
from troposphere.iam import Role, Policy, InstanceProfile, Ref
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add IAM roles to the given template
    """

    role = Role(
        Config.get_resource_name(config.get('name')),
        AssumeRolePolicyDocument=config.get('assumeRolePolicyDocument')
    )

    add_value(role, 'ManagedPolicyArns', config.get('managedPolicyArns', False))
    add_value(role, 'Path', config.get('path', False))

    policy_config = config.get('policies', False)

    if policy_config:

        policies = []
        for policy_data in policy_config:

            policies.append(
                Policy(
                    PolicyName=policy_data['policyName'],
                    PolicyDocument=policy_data['policyDocument']
                )
            )

        add_value(role, 'Policies', policies)

    template.add_resource(role)

"""Create Launch Configurations"""
from troposphere.autoscaling import LaunchConfiguration
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add Launch configurations to a given template
    """

    launch_configuration = LaunchConfiguration(
        Config.get_resource_name(config.get('name')),
        ImageId=config.get('imageId'),
        InstanceType=config.get('instanceType')
    )

    add_value(
        launch_configuration,
        "AssociatePublicIpAddress",
        config.get('associatePublicIpAddress', False)
    )
    add_value(launch_configuration, "BlockDeviceMappings", config.get('blockDeviceMappings', False))
    add_value(launch_configuration, "ClassicLinkVPCId", config.get('classicLinkVPCId', False))
    add_value(
        launch_configuration,
        "ClassicLinkVPCSecurityGroups",
        config.get('classicLinkVPCSecurityGroups', False)
    )
    add_value(launch_configuration, "EbsOptimized", config.get('ebsOptimized', False))
    add_value(launch_configuration, "IamInstanceProfile", config.get('iamInstanceProfile', False))
    add_value(launch_configuration, "InstanceId", config.get('instanceId', False))
    add_value(launch_configuration, "InstanceMonitoring", config.get('instanceMonitoring', False))
    add_value(launch_configuration, "KernelId", config.get('kernelId', False))
    add_value(launch_configuration, "KeyName", config.get('keyName', False))
    add_value(launch_configuration, "PlacementTenancy", config.get('placementTenancy', False))
    add_value(launch_configuration, "RamDiskId", config.get('ramDiskId', False))
    add_value(launch_configuration, "SecurityGroups", config.get('securityGroups', False))
    add_value(launch_configuration, "SpotPrice", config.get('spotPrice', False))
    add_value(launch_configuration, "UserData", config.get('userData', False))

    template.add_resource(launch_configuration)

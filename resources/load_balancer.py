"""Create load balancers"""
from troposphere.elasticloadbalancing import LoadBalancer, Listener
from troposphere.elasticloadbalancing import HealthCheck, ConnectionDrainingPolicy
from troposphere.elasticloadbalancing import AppCookieStickinessPolicy, ConnectionSettings
from troposphere.elasticloadbalancing import LBCookieStickinessPolicy, Policy
from troposphere.elasticloadbalancing import AccessLoggingPolicy
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add a load balancer to the template
    """
    name = config.get('loadBalancerName')

    listeners = []

    listeners_config = config.get('listeners')

    for listener_config in listeners_config:

        listener_config = Config('listener', listener_config, environment)

        listener = Listener(
            InstancePort=listener_config.get('instancePort'),
            LoadBalancerPort=listener_config.get('loadBalancerPort'),
            Protocol=listener_config.get('protocol')
        )

        add_value(listener, 'InstanceProtocol', listener_config.get('instanceProtocol', False))
        add_value(listener, 'PolicyNames', listener_config.get('policyNames', False))
        add_value(listener, 'SSLCertificateId', listener_config.get('sslCertificateId', False))

        listeners.append(listener)

    load_balancer = LoadBalancer(
        Config.get_resource_name(name),
        Listeners=listeners
    )

    access_logging_config = config.get('accessLoggingPolicy', False)

    if access_logging_config:

        access_logging_config = Config('accessLogging', access_logging_config, environment)

        access_logging_policy = AccessLoggingPolicy(
            Enabled=access_logging_config.get('enabled'),
            S3BucketName=access_logging_config.get('s3BucketName'),
        )

        add_value(
            access_logging_policy,
            'EmitInterval',
            access_logging_config.get('emitInterval', False)
        )

        add_value(
            access_logging_policy,
            'S3BucketPrefix',
            access_logging_config.get('s3BucketPrefix', False)
        )

        add_value(load_balancer, 'AccessLoggingPolicy', access_logging_policy)

    app_cookie_policies_config = config.get('appCookieStickinessPolicy', False)

    if app_cookie_policies_config:

        app_cookie_policies = []

        for app_cookie_policy_config in app_cookie_policies_config:

            app_cookie_policy_config = Config('appCookiePolicy', app_cookie_policy_config, environment)

            app_cookie_policies.append(
                AppCookieStickinessPolicy(
                    CookieName=app_cookie_policy_config.get('cookieName'),
                    PolicyName=app_cookie_policy_config.get('policyName')
                )
            )

        add_value(
            load_balancer,
            'AppCookieStickinessPolicy',
            app_cookie_policies
        )

    connection_settings = config.get('connectionSettings', False)

    if connection_settings:

        connection_settings = Config('connectionSettings', connection_settings, environment)

        add_value(
            load_balancer,
            'ConnectionSettings',
            ConnectionSettings(
                IdleTimeout=connection_settings.get('idleTimeout')
            )
        )

    health_check_config = config.get('healthCheck', False)

    if health_check_config:

        health_check_config = Config('healthCheckConfig', health_check_config, environment)

        health_check = HealthCheck(
            Target=health_check_config.get('target'),
            HealthyThreshold=health_check_config.get('healthyThreshold'),
            UnhealthyThreshold=health_check_config.get('unhealthyThreshold'),
            Interval=health_check_config.get('interval'),
            Timeout=health_check_config.get('timeout')
        )

        add_value(load_balancer, "HealthCheck", health_check)


    draining_policy_config = config.get('connectionDrainingPolicy', False)

    if draining_policy_config:

        draining_policy_config = Config('drainingPolicy', draining_policy_config, environment)

        draining_policy = ConnectionDrainingPolicy(
            Enabled=draining_policy_config.get('enabled')
        )

        add_value(
            draining_policy,
            'Timeout',
            draining_policy_config.get('timeout', False)
        )

        add_value(load_balancer, "ConnectionDrainingPolicy", draining_policy)

    lb_cookie_policies_config = config.get('lbCookieStickinessPolicy', False)

    if lb_cookie_policies_config:

        lb_cookie_policies = []

        for lb_cookie_policy_config in lb_cookie_policies_config:

            lb_cookie_policy_config = Config('lbCookiePolicy', lb_cookie_policy_config, environment)

            lb_cookie_policy = LBCookieStickinessPolicy(
                PolicyName=lb_cookie_policy_config.get('policyName')
            )

            add_value(
                lb_cookie_policy,
                "CookieExpirationPeriod",
                lb_cookie_policy_config.get('cookieExpirationPeriod', False)
            )

            lb_cookie_policies.append(lb_cookie_policy)

        add_value(
            load_balancer,
            "LBCookieStickinessPolicy",
            lb_cookie_policies
        )

    policies_config = config.get('policies', False)

    if policies_config:

        policies = []

        for policy_config in policies_config:

            policy_config = Config('policy', policy_config, environment)

            policy = Policy(
                Attributes=policy_config.get('attributes'),
                PolicyName=policy_config.get('policyName'),
                PolicyType=policy_config.get('policyType')
            )

            add_value(policy, 'InstancePorts', policy_config.get('instancePorts', False))

            policies.append(policy)

        add_value(load_balancer, 'Policies', policies)

    add_value(load_balancer, "AvailabilityZones", config.get('availabilityZones', False))
    add_value(load_balancer, "CrossZone", config.get('crossZone', False))
    add_value(load_balancer, "Instances", config.get('instances', False))
    add_value(load_balancer, "LoadBalancerName", name)
    add_value(load_balancer, "Scheme", config.get('scheme', False))
    add_value(load_balancer, "SecurityGroups", config.get('securityGroups', False))
    add_value(load_balancer, "Subnets", config.get('subnets', False))
    add_value(load_balancer, "Tags", config.get('tags', False))

    template.add_resource(load_balancer)

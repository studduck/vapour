"""Create opsworks stacks"""
from troposphere import GetAtt
from troposphere.iam import Role, Policy, InstanceProfile, Ref
from troposphere.opsworks import Stack, ChefConfiguration, Instance, ElasticLoadBalancerAttachment
from troposphere.opsworks import StackConfigurationManager, Source, App
from troposphere.opsworks import Layer, Recipes, TimeBasedAutoScaling
from troposphere.opsworks import LifeCycleConfiguration, ShutdownEventConfiguration
from troposphere.opsworks import LoadBasedAutoScaling, AutoScalingThresholds
from troposphere.opsworks import VolumeConfiguration
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add opsworks stack to the given template
    """

    stack_name = "OpsworksStack"

    for layer in config.get_all('layer'):

        add_layer(template, layer, stack_name, environment)

    template.add_resource(
        Role(
            config.get('stackIamRoleName'),
            Path="/",
            AssumeRolePolicyDocument={"Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": [
                            "opsworks.amazonaws.com"
                        ]
                    },
                    "Action": [
                        "sts:AssumeRole"
                    ]
                }
                ]},
            Policies=[
                Policy(
                    PolicyName=stack_name + '-opsworks-' + environment,
                    PolicyDocument={
                        "Statement": [{
                            "Effect": "Allow",
                            "Action": config.get('actions', False),
                            "Resource": "*"
                        }],
                    },
                )
            ]
        )
    )

    ec2_statements = []
    iam_ec2_policy_config = config.get('iamEc2PolicyStatements', False)

    if iam_ec2_policy_config:
        for policy_data in iam_ec2_policy_config:
            ec2_statements.append(policy_data)

    template.add_resource(
        Role(
            config.get('ec2IamRoleName'),
            Path="/",
            AssumeRolePolicyDocument={"Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": [
                            "ec2.amazonaws.com"
                        ]
                    },
                    "Action": [
                        "sts:AssumeRole"
                    ],
                }
            ]},
            Policies=[Policy(
                PolicyName='opsworks-ec2',
                PolicyDocument={"Statement": ec2_statements}
            )]
        )
    )


    template.add_resource(
        InstanceProfile(
            config.get('instanceProfileName'),
            Path="/",
            Roles=[Ref(config.get('ec2IamRoleName'))]
        )
    )

    for app in config.get_all('app'):
        template.add_resource(App(
            Config.get_resource_name(app.name),
            StackId=Ref(stack_name),
            Type=app.get('type'),
            Name=app.get('name'),
            Shortname=app.get('shortName'),
            AppSource=Source(
                Type=app.get('sourceType'),
                Url=app.get('sourceUrl'),
                Revision=app.get('sourceRevision')
            )
        ))


    opsworks_stack_resource = Stack(
        stack_name,
        DependsOn=[
            config.get('stackIamRoleName'),
            config.get('instanceProfileName'),
            config.get('ec2IamRoleName')
        ],
        DefaultInstanceProfileArn=GetAtt(config.get('instanceProfileName'), 'Arn'),
        Name=config.get('name'),
        ServiceRoleArn=GetAtt(config.get('stackIamRoleName'), 'Arn')
    )

    add_value(opsworks_stack_resource, 'AgentVersion', config.get('agentVersion', False))
    add_value(opsworks_stack_resource, 'Attributes', config.get('attributes', False))

    chef_configuration_config = config.get('chefConfiguration', False)

    if chef_configuration_config:

        add_chef_configuration(opsworks_stack_resource, chef_configuration_config ,environment)

    configuration_manager_config = config.get('configurationManager', False)

    if configuration_manager_config:

        add_configuration_manager(opsworks_stack_resource, configuration_manager_config, environment)

    custom_cookbooks_config = config.get('customCookbooksSource', False)

    if custom_cookbooks_config:

        add_custom_cookbooks(opsworks_stack_resource, custom_cookbooks_config, environment)

    add_value(opsworks_stack_resource, 'CustomJson', config.get('customJson', False))

    add_value(
        opsworks_stack_resource,
        'DefaultAvailabilityZone',
        config.get('defaultAvailabilityZone', False)
    )

    add_value(
        opsworks_stack_resource,
        'DefaultInstanceProfileArn',
        config.get('defaultInstanceProfileArn', False)
    )

    add_value(
        opsworks_stack_resource,
        'DefaultRootDeviceType',
        config.get('defaultRootDeviceType', False)
    )

    add_value(opsworks_stack_resource, 'DefaultOs', config.get('defaultOs', False))
    add_value(opsworks_stack_resource, 'DefaultSshKeyName', config.get('defaultSshKeyName', False))
    add_value(opsworks_stack_resource, 'DefaultSubnetId', config.get('defaultSubnetId', False))
    add_value(opsworks_stack_resource, 'HostnameTheme', config.get('hostnameTheme', False))

    add_value(
        opsworks_stack_resource,
        'UseCustomCookbooks',
        config.get('useCustomCookbooks', False)
    )

    add_value(
        opsworks_stack_resource,
        'UseOpsworksSecurityGroups',
        config.get('useOpsworksSecurityGroups', False)
    )

    add_value(opsworks_stack_resource, 'VpcId', config.get('vpcId', False))

    template.add_resource(opsworks_stack_resource)

    for instance in config.get_all('instance'):

        add_instance(template, instance, environment)

def add_layer(template, layer, stack_name, environment):
    """
    @template Template
    @config Config
    """

    layer_resource = Layer(
        Config.get_resource_name(layer.name),
        AutoAssignElasticIps=layer.get('autoAssignElasticIps'),
        AutoAssignPublicIps=layer.get('autoAssignPublicIps'),
        EnableAutoHealing=layer.get('enableAutoHealing'),
        Name=layer.get('name'),
        Shortname=layer.get('shortname'),
        StackId=Ref(stack_name),
        Type=layer.get('type')

    )

    add_value(layer_resource, 'Attributes', layer.get('attributes', False))
    add_value(
        layer_resource,
        'CustomInstanceProfileArn',
        layer.get('customIntanceProfileArn', False)
    )

    custom_recipes = layer.get('customRecipes', False)

    if custom_recipes:

        recipes = Recipes(
            Setup=custom_recipes['setup'] if 'setup' in custom_recipes else [],
            Configure=custom_recipes['configure'] if 'configure' in custom_recipes else [],
            Deploy=custom_recipes['deploy'] if 'deploy' in custom_recipes else [],
            Undeploy=custom_recipes['undeploy'] if 'undeploy' in custom_recipes else [],
            Shutdown=custom_recipes['shutdown'] if 'shutdown' in custom_recipes else [],
        )

        add_value(layer_resource, 'CustomRecipes', recipes)

    all_custom_security_groups = layer.get_all('customSecurityGroupIds')

    if all_custom_security_groups:

        custom_security_group_ids = []

        for security_group_id in all_custom_security_groups[0].get_all():
            security_group_id = Config('securityGroup', {'id': security_group_id}, environment)
            custom_security_group_ids.append(security_group_id.get('id'))

        add_value(layer_resource, 'CustomSecurityGroupIds', custom_security_group_ids)

    add_value(layer_resource, 'InstallUpdatesOnBoot', layer.get('installUpdatesOnBoot', False))

    life_cycle_event_config = layer.get('lifecycleEventConfiguration', False)

    if life_cycle_event_config:

        add_lifecycle_event_config(layer_resource, life_cycle_event_config, environment)

    load_based_auto_scaling_config = layer.get('loadBasedAutoScaling', False)

    if load_based_auto_scaling_config:

        add_load_based_auto_scaling(layer_resource, load_based_auto_scaling_config, environment)

    add_value(layer_resource, 'Packages', layer.get('packages', False))

    volume_config_values = layer.get('volumeConfigurations', False)

    if volume_config_values:

        volume_configs = []

        for volume_config_value in volume_config_values:

            volume_config_value = Config('volumeConfig', volume_config_value, environment)

            volume_configuration = VolumeConfiguration(
                MountPoint=volume_config_value.get('mountPoint'),
                NumberOfDisks=volume_config_value.get('numberOfDisks'),
                Size=volume_config_value.get('size')
            )

            add_value(volume_configuration, 'Iops', volume_config_value.get('iops', False))
            add_value(
                volume_configuration,
                'RaidLevel',
                volume_config_value.get('raidLevel', False)
            )

            add_value(volume_configuration, 'VolumeType', volume_config_value.get('volumeType'))

            volume_configs.append(volume_configuration)

        add_value(layer_resource, 'VolumeConfigurations', volume_configs)


    elb_values = layer.get('elbAttachment', False)

    if elb_values:

        template.add_resource(
            ElasticLoadBalancerAttachment(
                Config.get_resource_name(layer.name + 'elb'),
                ElasticLoadBalancerName=elb_values.get('elbName'),
                LayerId=GetAtt(Config.get_resource_name(layer.name), 'LayerId')
            )
        )

    template.add_resource(layer_resource)

def add_lifecycle_event_config(layer, life_cycle_event_config, environment):
    """
    @layer layer Layer
    @life_cycle_event_config Config
    """

    life_cycle_event_config = Config('lifeCycleEvent', life_cycle_event_config, environment)

    lifecycle_config = LifeCycleConfiguration('lifecycleConfig')

    shutdown_event_config = life_cycle_event_config.get('shutdownEventConfiguration', False)

    if shutdown_event_config:

        shutdown_event_config = Config('shutdownEvent', shutdown_event_config, environment)

        shutdown_event = ShutdownEventConfiguration('shutdownEvent')

        add_value(
            shutdown_event,
            'DelayUntilElbConnectionsDrained',
            shutdown_event_config.get('delayUntilElbConnectionsDrained', False)
        )

        add_value(
            shutdown_event,
            'ExecutionTimeout',
            shutdown_event_config.get('executionTimeout', False)
        )

        add_value(lifecycle_config, 'ShutdownEventConfiguration', shutdown_event)

    add_value(layer, 'LifecycleEventConfiguration', lifecycle_config)

def add_load_based_auto_scaling(layer, load_config, environment):
    """
    @layer Layer
    @load_config Config
    """

    load_config = Config('load_config', load_config, environment)

    load_resource = LoadBasedAutoScaling()

    add_value(load_resource, 'Enable', load_config.get('enable', False))

    down_scaling_config = load_config.get('downScaling', False)

    if down_scaling_config:
        add_value(
            load_resource,
            'DownScaling',
            AutoScalingThresholds(
                CpuThreshold=down_scaling_config.get('cpuThreshold', False),
                IgnoreMetricsTime=down_scaling_config.get('ignoreMetricsTime', False),
                InstanceCount=down_scaling_config.get('instanceCount'),
                LoadThreshold=down_scaling_config.get('loadThreshold'),
                MemoryThreshold=down_scaling_config.get('memoryThreshold'),
                ThresholdsWaitTime=down_scaling_config.get('thresholdsWaitTime')
            )
        )

    up_scaling_config = load_config.get('upScaling', False)

    if up_scaling_config:
        add_value(
            load_resource,
            'UpScaling',
            AutoScalingThresholds(
                CpuThreshold=up_scaling_config.get('cpuThreshold', False),
                IgnoreMetricsTime=up_scaling_config.get('ignoreMetricsTime', False),
                InstanceCount=up_scaling_config.get('instanceCount'),
                LoadThreshold=up_scaling_config.get('loadThreshold'),
                MemoryThreshold=up_scaling_config.get('memoryThreshold'),
                ThresholdsWaitTime=up_scaling_config.get('thresholdsWaitTime')
            )
        )

    add_value(layer, 'LoadBasedAutoScaling', load_resource)

def add_instance(template, instance, environment):
    """
    @template Template
    @instance Config
    """

    instance_resource = Instance(
        Config.get_resource_name(instance.name),
        InstanceType=instance.get('instanceType'),
        LayerIds=instance.get('layerIds'),
        StackId=instance.get('stackId')
    )

    add_value(instance_resource, 'AmiId', instance.get('amiId', False))
    add_value(instance_resource, 'Architecture', instance.get('architecture', False))
    add_value(instance_resource, 'AutoScalingType', instance.get('autoScalingType', False))
    add_value(instance_resource, 'AvailabilityZone', instance.get('availabilityZone', False))
    # add_value(instance_resource, 'EbsOptimized', instance.get('ebsOptimized', False))
    # UNSUPPORTED BY TROPOSHERE AT VERSION 1.3.0

    add_value(
        instance_resource,
        'InstallUpdatesOnBoot',
        instance.get('installUpdatesOnBoot', False)
    )

    add_value(instance_resource, 'Os', instance.get('os', False))
    add_value(instance_resource, 'RootDeviceType', instance.get('rootDeviceType', False))
    add_value(instance_resource, 'SshKeyName', instance.get('sshKeyName', False))
    add_value(instance_resource, 'SubnetId', instance.get('subnetId', False))

    timings = instance.get('timeBasedAutoScaling', False)

    if timings:

        time_based_resource = TimeBasedAutoScaling()

        for day, timing in timings.items():
            add_value(time_based_resource, day, timing)

        add_value(instance_resource, 'TimeBasedAutoScaling', time_based_resource)

    template.add_resource(instance_resource)

def add_chef_configuration(opsworks_stack, chef_configuration_config, environment):
    """
    @opsworks_stack Stack
    @chef_configuration_config Config
    """

    chef_configuration_config = Config('chef_config', chef_configuration_config, environment)

    chef_configuration = ChefConfiguration()

    add_value(
        chef_configuration,
        'BerkshelfVersion',
        chef_configuration_config.get('berkshelfVersion', False)
    )

    add_value(
        chef_configuration,
        'ManageBerkshelf',
        chef_configuration_config.get('manageBerkshelf', False)
    )

    add_value(opsworks_stack, 'ChefConfiguration', chef_configuration)

def add_configuration_manager(opsworks_stack, configuration_manager_config, environment):
    """
    @opsworks_stack Stack
    @configuration_manager_config Config
    """

    configuration_manager_config = Config('configManager', configuration_manager_config, environment)

    configuration_manager = StackConfigurationManager()

    add_value(configuration_manager, 'Name', configuration_manager_config.get('name', False))

    add_value(
        configuration_manager,
        'Version',
        configuration_manager_config.get('version', False)
    )

    add_value(opsworks_stack, 'ConfigurationManager', configuration_manager)

def add_custom_cookbooks(opsworks_stack, custom_cookbooks_config, environment):
    """
    @opsworks_stack Stack
    @custom_cookbooks_config Config
    """

    custom_cookbooks_config = Config('customCookbooks', custom_cookbooks_config, environment)

    custom_cookbooks = Source()

    add_value(custom_cookbooks, 'Password', custom_cookbooks_config.get('password', False))
    add_value(custom_cookbooks, 'Revision', custom_cookbooks_config.get('revision', False))
    add_value(custom_cookbooks, 'SshKey', custom_cookbooks_config.get('sshKey', False))
    add_value(custom_cookbooks, 'Type', custom_cookbooks_config.get('type', False))
    add_value(custom_cookbooks, 'Url', custom_cookbooks_config.get('url', False))
    add_value(custom_cookbooks, 'Username', custom_cookbooks_config.get('username', False))

    add_value(opsworks_stack, 'CustomCookbooksSource', custom_cookbooks)

"""Create parameters"""
from troposphere import Parameter
from resource import add_value
from config import Config

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add parameter to the template
    """

    parameter = Parameter(
        Config.get_resource_name(config.get('name')),
        Type=config.get('type'),
    )

    add_value(parameter, 'Default', config.get('default', False))
    add_value(parameter, 'NoEcho', config.get('noEcho', False))
    add_value(parameter, 'AllowedValues', config.get('allowedValues', False))
    add_value(parameter, 'AllowedPattern', config.get('allowedPattern', False))
    add_value(parameter, 'MaxLength', config.get('maxLength', False))
    add_value(parameter, 'MinLength', config.get('minLength', False))
    add_value(parameter, 'MaxValue', config.get('maxValue', False))
    add_value(parameter, 'MinValue', config.get('minValue', False))
    add_value(parameter, 'Description', config.get('description', False))
    add_value(parameter, 'ConstraintDescription', config.get('constraintDescription', False))

    template.add_parameter(parameter)

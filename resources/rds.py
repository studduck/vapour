"""Create RDS stacks"""
from troposphere import GetAtt, Parameter
from troposphere.iam import Ref
from troposphere.ec2 import SecurityGroup
from troposphere.rds import DBInstance
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add a RDS instance to the given template
    """

    db_name = config.get('dbInstanceIdentifier')

    db_instance = DBInstance(
        config.get_resource_name(db_name),
        DBInstanceClass=config.get('dbInstanceClass'),
        DBInstanceIdentifier=db_name
    )

    add_value(db_instance, 'AllocatedStorage', config.get('allocatedStorage', False))
    add_value(
        db_instance,
        'AllowMajorVersionUpgrade',
        config.get('allowMajorVersionUpgrade', False)
    )
    add_value(db_instance, 'AutoMinorVersionUpgrade', config.get('autoMinorVersionUpgrade', False))
    add_value(db_instance, 'AvailabilityZone', config.get('availabilityZone', False))
    add_value(db_instance, 'BackupRetentionPeriod', config.get('backupRetentionPeriod', False))
    add_value(db_instance, 'CharacterSetName', config.get('characterSetName', False))
    add_value(db_instance, 'DBClusterIdentifier', config.get('dbClusterIdentifier', False))
    add_value(db_instance, 'DBName', config.get('dbName', False))
    add_value(db_instance, 'DBParameterGroupName', config.get('dbParameterGroupName', False))
    add_value(db_instance, 'DBSecurityGroups', config.get('dbSecurityGroups', False))
    add_value(db_instance, 'DBSnapshotIdentifier', config.get('dbSnapshotIdentifier', False))
    add_value(db_instance, 'DBSubnetGroupName', config.get('dbSubnetGroupName', False))
    add_value(db_instance, 'Engine', config.get('engine', False))
    add_value(db_instance, 'EngineVersion', config.get('engineVersion', False))
    add_value(db_instance, 'Iops', config.get('iops', False))
    add_value(db_instance, 'KmsKeyId', config.get('kmsKeyId', False))
    add_value(db_instance, 'LicenseModel', config.get('licenseModel', False))
    add_value(db_instance, 'MasterUsername', config.get('masterUsername', False))
    add_value(db_instance, 'MasterUserPassword', config.get('masterUserPassword', False))
    add_value(db_instance, 'MultiAZ', config.get('multiAz', False))
    add_value(db_instance, 'OptionGroupName', config.get('optionGroupName', False))
    add_value(db_instance, 'Port', config.get('port', False))
    add_value(db_instance, 'PreferredBackupWindow', config.get('preferredBackupWindow', False))
    add_value(
        db_instance,
        'PreferredMaintenanceWindow',
        config.get('preferredMaintenanceWindow', False)
    )
    add_value(db_instance, 'PubliclyAccessible', config.get('publiclyAccessible', False))
    add_value(
        db_instance,
        'SourceDBInstanceIdentifier',
        config.get('sourceDBInstanceIdentifier', False)
    )
    add_value(db_instance, 'StorageEncrypted', config.get('storageEncrypted', False))
    add_value(db_instance, 'StorageType', config.get('storageType', False))
    add_value(db_instance, 'Tags', config.get('tags', False))
    add_value(db_instance, 'VPCSecurityGroups', config.get('vpcSecurityGroups', False))

    template.add_resource(db_instance)

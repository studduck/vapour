"""Create route53 record sets"""
from troposphere.route53 import RecordSetType, AliasTarget, GeoLocation
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @stack_name The name of the stack
    @environment The environment (dev,uat,prod)
    @template Template
    @config: Config
    Add route 53 record sets
    """

    record = RecordSetType(
        Config.get_resource_name(config.get('name')+'RecordSet'),
        Name=config.get('name'),
        Type=config.get('type')
    )

    alias_target_config = config.get('aliasTarget', False)

    if alias_target_config:

        alias_target_config = Config('aliasTarget', alias_target_config, environment)

        alias_target = AliasTarget(
            dnsname=alias_target_config.get('dnsName'),
            hostedzoneid=alias_target_config.get('hostedZoneId')
        )

        add_value(
            alias_target,
            'EvaluateTargetHealth',
            alias_target_config.get('evaluateTargetHealth', False)
        )

        add_value(record, 'AliasTarget', alias_target)

    add_value(record, 'Failover', config.get('failover', False))

    geo_location_config = config.get('geoLocation', False)

    if geo_location_config:

        geo_location_config = Config('geoLocation', geo_location_config, environment)

        geo_location = GeoLocation()

        add_value(geo_location, 'ContinentCode', geo_location_config.get('continentCode', False))
        add_value(geo_location, 'CountryCode', geo_location_config.get('countryCode', False))
        add_value(
            geo_location,
            'SubdivisionCode',
            geo_location_config.get('subdivisionCode', False)
        )

        add_value(record, 'GeoLocation', geo_location)

    add_value(record, 'HealthCheckId', config.get('healthCheckId', False))
    add_value(record, 'HostedZoneId', config.get('hostedZoneId', False))
    add_value(record, 'HostedZoneName', config.get('hostedZoneName', False))
    add_value(record, 'Region', config.get('region', False))
    add_value(record, 'ResourceRecords', config.get('resourceRecords', False))
    add_value(record, 'SetIdentifier', config.get('setIdentifier', False))
    add_value(record, 'TTL', config.get('ttl', False))
    add_value(record, 'Weight', config.get('weight', False))

    template.add_resource(record)

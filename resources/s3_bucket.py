"""Create s3 buckets"""
from troposphere.s3 import *
from troposphere import s3
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @stack_name The name of the stack
    @environment The environment (dev,uat,prod)
    @template Template
    @config: Config
    Add s3 buckets to the given template
    """

    bucket_name = config.get('bucketName')

    bucket = Bucket(config.get_resource_name(bucket_name)+'bucket')

    add_value(bucket, 'AccessControl', config.get('accessControl', False))
    add_value(bucket, 'BucketName', bucket_name)

    cors_configuration_config = config.get('corsConfiguration', False)

    if cors_configuration_config:

        cors_configuration_config = Config('corsConfig', cors_configuration_config, environment)
        cors_rules = []

        cors_rules_config = cors_configuration_config.get('corsRules')

        for cors_rule_config in cors_rules_config:

            cors_rule_config = Config('corsRules', cors_rule_config, environment)

            cors_rule = CorsRules(
                AllowedOrigins=cors_rule_config.get('allowedOrigins'),
                AllowedMethods=cors_rule_config.get('allowedMethods')
            )

            add_value(cors_rule, 'AllowedHeaders', cors_rule_config.get('allowedHeaders', False))
            add_value(cors_rule, 'ExposedHeaders', cors_rule_config.get('exposedHeaders', False))
            add_value(cors_rule, 'Id', cors_rule_config.get('id', False))
            add_value(cors_rule, 'MaxAge', cors_rule_config.get('maxAge', False))

            cors_rules.append(cors_rule)

        cors_configuration = CorsConfiguration(
            CorsRules=cors_rules
        )

        add_value(bucket, 'CorsConfiguration', cors_configuration)

    lifecycle_configuration_config = config.get('lifecycleConfiguration', False)

    if lifecycle_configuration_config:

        lifecycle_configuration_config = Config('lifecycleConfig', lifecycle_configuration_config, environment)

        rules = []

        rules_config = lifecycle_configuration_config.get('rules')

        for rule_config in rules_config:

            rule_config = Config('rule', rule_config, environment)

            rule = LifecycleRule(
                Status=rule_config.get('status')
            )

            add_value(rule, 'ExpirationDate', rule_config.get('expirationDate', False))
            add_value(rule, 'ExpirationInDays', rule_config.get('expirationInDays', False))
            add_value(rule, 'Id', rule_config.get('id', False))

            add_value(
                rule,
                'NoncurrentVersionExpirationInDays',
                rule_config.get('noncurrentVersionExpirationInDays', False)
            )

            noncurrent_version_config = rule_config.get('noncurrentVersionTransition', False)

            if noncurrent_version_config:

                noncurrent_version_config = Config('noncurrentVersion', noncurrent_version_config, environment)

                add_value(
                    rule,
                    'NonCurrentVersionTransition',
                    NoncurrentVersionTransition(
                        StorageClass=noncurrent_version_config.get('storageClass'),
                        TransitionInDays=noncurrent_version_config.get('transitionInDays')
                    )
                )

            add_value(rule, 'Prefix', rule_config.get('prefix', False))
            add_value(rule, 'Status', rule_config.get('status', False))

            lifecycle_transition_config = rule_config.get('transition', False)

            if lifecycle_transition_config:

                lifecycle_transition_config = Config(
                    'lifecycleTransition',
                    lifecycle_transition_config,
                    environment
                )

                lifecycle_transition = LifecycleRuleTransition(
                    StorageClass=lifecycle_transition_config.get('storageClass')
                )

                add_value(
                    lifecycle_transition,
                    'TransitionDate',
                    lifecycle_transition_config.get('transitionDate', False)
                )

                add_value(
                    lifecycle_transition,
                    'TransitionInDays',
                    lifecycle_transition_config.get('transitionInDays', False)
                )

                add_value(rule, 'Transition', lifecycle_transition)

            rules.append(rule)

        add_value(
            bucket,
            'LifecycleConfiguration',
            LifecycleConfiguration(
                Rules=rules
            )
        )

    logging_configuration_config = config.get('loggingConfiguration', False)

    if logging_configuration_config:

        logging_configuration_config = Config('loggingConfig', logging_configuration_config, environment)

        logging_configuration = LoggingConfiguration()

        add_value(
            logging_configuration,
            'DestinationBucketName',
            logging_configuration_config.get('destinationBucketName', False)
        )

        add_value(
            logging_configuration,
            'LogFilePrefix',
            logging_configuration_config.get('logFilePrefix', False)
        )

        add_value(bucket, 'LoggingConfiguration', logging_configuration)

    notification_config = config.get('notificationConfiguration', False)

    if notification_config:

        notification_config = Config('notificationConfig', notification_config, environment)

        notification_configuration = NotificationConfiguration()

        lambda_configurations_config = notification_config.get('lambdaConfigurations', False)

        if lambda_configurations_config:

            add_configuration(
                notification_configuration,
                lambda_configurations_config,
                'Lambda',
                environment
            )

        queue_configurations_config = config.get('queueConfigurations', False)

        if queue_configurations_config:

            add_configuration(
                notification_configuration,
                queue_configurations_config,
                'Queue',
                environment
            )

        topic_configurations_config = config.get('topicConfigurations', False)

        if topic_configurations_config:

            add_configuration(
                notification_configuration,
                queue_configurations_config,
                'Topic',
                environment
            )

        add_value(bucket, 'NotificationConfiguration', notification_configuration)

    replication_config = config.get('replicationConfiguration', False)

    if replication_config:

        replication_config = Config('replicationConfig', replication_config, environment)

        replication_configuration = ReplicationConfiguration(
            Role=replication_config.get('role')
        )

        rules_config = replication_config.get('rules')

        rules = []

        for rule_config in rules_config:

            replication_rule = ReplicationConfigurationRules(
                Destination=rule_config.get('destination'),
                Prefix=rule_config.get('prefix'),
                Status=rule_config.get('status')
            )

            add_value(replication_rule, 'Id', rule_config.get('id', False))

            rules.append(replication_rule)

        add_value(replication_configuration, 'Rules', rules)
        add_value(bucket, 'ReplicationConfiguration', replication_configuration)

    tags_config = config.get('tags', False)

    if tags_config:

        tags = Tags()

        tags.tags = tags_config

        add_value(bucket, 'Tags', tags)

    versioning_config = config.get('versioningConfiguration', False)

    if versioning_config:

        versioning_config = Config('versioningConfig', versioning_config, environment)

        add_value(
            bucket,
            'VersioningConfiguration',
            VersioningConfiguration(Status=versioning_config.get('status'))
        )

    website_config = config.get('websiteConfiguration', False)

    if website_config:

        website_config = Config('website', website_config, environment)

        website_configuration = WebsiteConfiguration(
            IndexDocument=website_config.get('indexDocument')
        )

        add_value(
            website_configuration,
            'ErrorDocument',
            website_config.get('ErrorDocument', False)
        )

        redirect_config = website_config.get('redirectAllRequestsTo', False)

        if redirect_config:

            redirect_config = Config('redirectConfig', redirect_config, environment)

            redirect = RedirectAllRequestsTo(
                HostName=redirect_config.get('hostName')
            )

            add_value(redirect, 'Protocol', redirect_config.get('protocol', False))

            add_value(
                website_configuration,
                'RedirectAllRequestsTo',
                redirect
            )

        routing_rules_configs = website_config.get('routingRules')

        if routing_rules_configs:

            routing_rules = []

            for routing_rules_config in routing_rules_configs:

                routing_rules_config = Config('routingRules', routing_rules_config, environment)

                redirect_rule_config = Config(
                    'redirectRule',
                    routing_rules_config.get('redirectRule'),
                    environment
                )

                redirect_rule = RedirectRule()

                add_value(redirect_rule, 'HostName', redirect_rule_config.get('hostName', False))

                add_value(
                    redirect_rule,
                    'HttpRedirectCode',
                    redirect_rule_config.get('httpRedirectCode', False)
                )

                add_value(redirect_rule, 'Protocol', redirect_rule_config.get('protocol', False))

                add_value(
                    redirect_rule,
                    'ReplaceKeyPrefixWith',
                    redirect_rule_config.get('replaceKeyPrefixWith', False)
                )

                add_value(
                    redirect_rule,
                    'ReplaceKeyWith',
                    redirect_rule_config.get('replaceKeyWith', False)
                )

                routing_rule = RoutingRule(
                    RedirectRule=redirect_rule
                )

                routing_rule_config = routing_rules_config.get('routingRuleCondition', False)

                if routing_rule_config:

                    routing_rule_config = Config('routingRule', routing_rule_config, environment)

                    routing_rule_condition = RoutingRuleCondition()

                    add_value(
                        routing_rule_condition,
                        'HttpErrorCodeReturnedEquals',
                        routing_rule_config.get('httpErrorCodeReturnedEquals', False)
                    )

                    add_value(
                        routing_rule_condition,
                        'KeyPrefixEquals',
                        routing_rule_config.get('keyPrefixEquals', False)
                    )

                    add_value(routing_rules, 'RoutingRuleCondition', routing_rule)

                routing_rules.append(routing_rule)

            add_value(
                website_configuration,
                'RoutingRules',
                routing_rules
            )

        add_value(bucket, 'WebsiteConfiguration', website_configuration)

    template.add_resource(bucket)


def add_configuration(notification_configuration, configurations_config, config_type, environment):
    """
    @notification_configuration NotificationConfiguration
    @configurations_config [Config]
    @type string Lamda, Queue or Topic
    Adds the corresponding configuration type to the parent config
    """

    configurations = []

    for configuration_config in configurations_config:

        configuration_config = Config('configuration', configuration_config, environment)

        class_type = getattr(s3, config_type + 'Configurations')

        configuration = class_type(
            Event=configuration_config.get('event'),
            Function=configuration_config.get('function')
        )

        filter_config = configuration_config.get('filter', False)

        if filter_config:

            filter_config = Config('filter', filter_config, environment)

            s3_key_config = Config('s3Key', filter_config.get('s3Key'), environment)

            rules = []

            rules_config = s3_key_config.get('rules')

            for rule_config in rules_config:
                rules.append(
                    Rules(
                        Name=rule_config.get('name'),
                        Value=rule_config.get('value')
                    )
                )

            filter_resource = Filter(
                S3Key=S3Key(
                    Rules=rules
                )
            )

            add_value(configuration, 'Filter', filter_resource)

        configurations.append(configuration)

    add_value(
        notification_configuration,
        config_type + 'Configurations',
        configurations
    )

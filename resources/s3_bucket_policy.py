"""Create s3 bucket policy"""
from troposphere.s3 import BucketPolicy
from config import Config

def add(stack_name, environment, template, config):
    """
    @stack_name The name of the stack
    @environment The environment (dev,uat,prod)
    @template Template
    @config: Config
    Add s3 bucket policy to the given template
    """

    bucket_name = config.get('bucket')

    template.add_resource(BucketPolicy(
        config.get_resource_name(bucket_name)+'bucketpolicy',
        Bucket=bucket_name,
        PolicyDocument=config.get('policyDocument')
    ))

"""Create a Scaling policy"""
from troposphere.autoscaling import ScalingPolicy, StepAdjustments
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add a scaling policy to the given template
    """

    scaling_policy = ScalingPolicy(
        Config.get_resource_name(config.get('name')),
        AdjustmentType=config.get('adjustmentType'),
        AutoScalingGroupName=config.get('autoScalingGroupName')
    )

    add_value(scaling_policy, 'Cooldown', config.get('cooldown', False))

    add_value(
        scaling_policy,
        'EstimatedInstanceWarmup',
        config.get('estimatedInstanceWarmup', False)
    )

    add_value(scaling_policy, 'MetricAggregationType', config.get('metricAggregationType', False))
    add_value(scaling_policy, 'MinAdjustmentMagnitude', config.get('minAdjustmentMagnitude', False))
    add_value(scaling_policy, 'PolicyType', config.get('policyType', False))
    add_value(scaling_policy, 'scalingAdjustment', config.get('scalingAdjustment', False))


    step_adjustments_configs = config.get('stepAdjustments', False)

    if step_adjustments_configs:

        step_adjustments = []

        for step_adjustments_config in step_adjustments_configs:

            step_adjustments_config = Config('stepAdjustment', step_adjustments_config, environment)

            step_adjustment = StepAdjustments(
                ScalingAdjustment=step_adjustments_config.get('scalingAdjustment')
            )

            add_value(
                step_adjustment,
                'MetricIntervalLowerBound',
                step_adjustments_config.get('metricIntervalLowerBound', False)
            )

            add_value(
                step_adjustment,
                'MetricIntervalUpperBound',
                step_adjustments_config.get('metricIntervalUpperBound', False)
            )

            step_adjustments.append(step_adjustment)

        add_value(scaling_policy, 'StepAdjustments', step_adjustments)

    template.add_resource(scaling_policy)

"""Create security groups"""
from troposphere.ec2 import SecurityGroup
from config import Config
import re
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add opsworks stack to the given template
    """

    security_group = SecurityGroup(
        Config.get_resource_name(config.get('name')),
        GroupDescription=config.get('groupDescription'),
        VpcId=config.get('vpcId')
    )

    rules = []

    ingress_rules = config.get('securityGroupIngress', False)

    if ingress_rules:

        for ingress_rule in ingress_rules:
            ingress_rule = Config('ingressRule', ingress_rule, environment)

            rule = {
                'FromPort': ingress_rule.get('fromPort'),
                'IpProtocol': ingress_rule.get('ipProtocol'),
                'ToPort': ingress_rule.get('toPort')
            }

            if ingress_rule.get('cidrIp', False):
                rule['CidrIp'] = ingress_rule.get('cidrIp')

            if ingress_rule.get('sourceSecurityGroupId', False):
                rule['SourceSecurityGroupId'] = ingress_rule.get('sourceSecurityGroupId')

            if ingress_rule.get('sourceSecurityGroupName', False):
                rule['SourceSecurityGroupName'] = ingress_rule.get('sourceSecurityGroupName')

            if ingress_rule.get('sourceSecurityGroupOwnerId', False):
                rule['SourceSecurityGroupOwnerId'] = ingress_rule.get('sourceSecurityGroupOwnerId')

            rules.append(rule)

        add_value(security_group, 'SecurityGroupIngress', rules)


    rules = []

    egress_rules = config.get('securityGroupEgress', False)

    if egress_rules:

        for egress_rule in egress_rules:
            egress_rule = Config('egressRule', egress_rule, environment)

            rule = {
                'FromPort': egress_rule.get('fromPort'),
                'IpProtocol': egress_rule.get('ipProtocol'),
                'ToPort': egress_rule.get('toPort')
            }

            if egress_rule.get('cidrIp', False):
                rule['CidrIp'] = egress_rule.get('cidrIp')

            if egress_rule.get('destinationSecurityGroupId', False):
                rule['DestinationSecurityGroupId'] = egress_rule.get('destinationSecurityGroupId')

            rules.append(rule)

        add_value(security_group, 'SecurityGroupEgress', rules)

    add_value(security_group, 'Tags', config.get('tags', False))

    template.add_resource(
        security_group
    )

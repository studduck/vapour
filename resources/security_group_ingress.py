"""Create security group ingress objects"""
from troposphere.ec2 import SecurityGroupIngress
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @template Template
    @config: Config
    Add security group ingress to the given template
    """

    security_group_ingress = SecurityGroupIngress(
        config.get_resource_name(config.get('name')),
        IpProtocol=config.get('ipProtocol')
    )

    add_value(security_group_ingress, 'CidrIp', config.get('cidrIp', False))
    add_value(security_group_ingress, 'FromPort', config.get('fromPort', False))
    add_value(security_group_ingress, 'GroupId', config.get('groupId', False))
    add_value(security_group_ingress, 'GroupName', config.get('groupName', False))

    add_value(
        security_group_ingress,
        'SourceSecurityGroupName',
        config.get('sourceSecurityGroupName', False)
    )

    add_value(
        security_group_ingress,
        'SourceSecurityGroupId',
        config.get('sourceSecurityGroupId', False)
    )

    add_value(
        security_group_ingress,
        'SourceSecurityGroupOwnerId',
        config.get('sourceSecurityGroupOwnerId', False)
    )

    add_value(security_group_ingress, 'ToPort', config.get('toPort', False))

    template.add_resource(security_group_ingress)

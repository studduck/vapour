"""Create SQS queues"""
from troposphere.sqs import Queue, RedrivePolicy
from config import Config
from resource import add_value

def add(stack_name, environment, template, config):
    """
    @stack_name The name of the stack
    @environment The environment (dev,uat,prod)
    @template Template
    @config: Config
    Add a SQS Queue to the given template
    """

    queue_name = config.get('queueName')

    queue = Queue(config.get_resource_name(queue_name)+'Queue')

    add_value(queue, 'QueueName', queue_name)
    add_value(queue, 'DelaySeconds', config.get('delaySeconds', False))
    add_value(queue, 'MaximumMessageSize', config.get('maximumMessageSize', False))
    add_value(queue, 'MessageRetentionPeriod', config.get('messageRetentionPeriod', False))
    add_value(
        queue,
        'ReceiveMessageWaitTimeSeconds',
        config.get('receiveMessageWaitTimeSeconds', False)
    )
    add_value(queue, 'VisibilityTimeout', config.get('visibilityTimeout', False))

    redrive_policy_config = config.get('redrivePolicy', False)

    if redrive_policy_config != None:
        redrive_policy_name = queue_name + 'redrivePolicy'
        redrive_policy_config = Config(redrive_policy_name, redrive_policy_config, environment)
        redrive_policy = RedrivePolicy()

        add_value(
            redrive_policy,
            'deadLetterTargetArn',
            redrive_policy_config.get('deadLetterTargetArn', False)
        )
        add_value(
            redrive_policy,
            'maxReceiveCount',
            redrive_policy_config.get('maxReceiveCount')
        )

        add_value(queue, 'RedrivePolicy', redrive_policy)


    template.add_resource(queue)

"""Builds a cloudformation template from a config dictionary"""
from config import Config

def build(template, name, config, output_directory_name, file_handler, resource_handler, config_factory):
    """Builds a template from a given config"""
    try:
        version = str(config.get('version'))
    except ValueError:
        version = "2010-09-09"

    template.add_version(version)
    template.add_description(config.get('description'))
    environment = config.get('environment').lower()

    directory = output_directory_name + '/' + environment
    file_name = directory + '/' + name + '.template'

    print('\n' + 'Building ' + file_name + '...')

    if config.get('resources') is not None:
        for resource, resource_values in config.get('resources').items():
            resource_config = config_factory.build(resource, resource_values, environment)
            resource_name = Config.get_module_key_from_resource(resource)

            resource_handler.add(
                resource_name,
                environment,
                template,
                resource_config
            )

    file_handler.write(
        file_name,
        directory,
        template.to_json())

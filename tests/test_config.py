"""Unit tests for the config module"""
import unittest
from config import Config, ConfigFactory
import os
from troposphere import Ref, GetAtt

class TestConfig(unittest.TestCase):
    """Test the config class"""

    def test_get(self):
        """Test the get method"""
        config = Config('test_config', {'key': 'value'}, 'test')
        self.assertEqual('value', config.get('key'))

    def test_validate(self):
        """Test validation raises error"""
        config = Config('test_config', {}, 'test')
        with self.assertRaises(ValueError):
            config.get('key')

    def test_get_all(self):
        """Test get all returns all"""
        values = {'key1': 'value1', 'key2': 'value2'}
        config = Config('test_config', values, 'test')
        self.assertEqual(values, config.get_all())

    def test_get_all_by_type(self):
        """Test that filtering by key returns new configs"""
        values = {
            'resource': 'value1',
            'other_resource': 'value2',
            'same__resource': 'value3'
        }

        config = Config('test_config', values, 'test')

        expected_results = ['value1', 'value3']
        results = []

        for result_config in config.get_all('resource'):
            results.append(result_config.get_all())

        self.assertEqual(sorted(expected_results), sorted(results))

    def test_get_module_key(self):
        """Test getting a module key from a resource"""

        value = 'name__module'
        self.assertEqual(Config.get_module_key_from_resource(value), 'module')

    def test_get_reference(self):
        """Test getting a ref value from a param"""

        value = 'Ref__value'
        config = Config('attr', {'param': value}, 'test')
        result = config.get('param')

        self.assertTrue(isinstance(result, Ref))
        self.assertEqual(result.data['Ref'], 'value')

    def test_get_attr(self):
        """Test getting a attr value from a param"""

        value = 'Att__key:value'
        config = Config('attr', {'param': value}, 'test')
        result = config.get('param')

        self.assertTrue(isinstance(result, GetAtt))
        self.assertEqual(result.data['Fn::GetAtt'], ['key', 'value'])

    def test_get_attr_list(self):
        """Get list attribute"""
        value = ['Ref__value']
        config = Config('attr', {'param': value}, 'test')
        result = config.get('param')[0]

        self.assertTrue(isinstance(result, Ref))
        self.assertEqual(result.data['Ref'], 'value')

class TestConfigFactory(unittest.TestCase):
    """Test the config factory class"""
    __TEST_CONFIG_DIR = os.path.dirname(os.path.realpath(__file__))+ '/config'

    def test_config_is_merged_with_base(self):
        """Test that the config is loaded and combined with the base"""

        file_path = self.__TEST_CONFIG_DIR + '/merge'

        expected_results = {
            'name': 'test stack file',
            'description': 'test stack file',
            'environment': 'test',
            'resources': {
                'test_resource': {
                    'param': 'value',
                    'base_param': 'base_value'
                }
            }
        }

        results = ConfigFactory().load_config_file_from_path(file_path)

        self.assertEqual(expected_results, results['test'][0].get_all())

    def test_config_is_merged_with_multiple_environments(self):
        """Test that the config is loaded and combined with the base"""

        file_path = self.__TEST_CONFIG_DIR + '/multiple_environments'

        expected_test_results = {
            'name': 'test stack file',
            'description': 'test stack file',
            'environment': 'test',
            'resources': {
                'test_resource': {
                    'param': 'value',
                    'base_param': 'base_test_value'
                }
            }
        }

        expected_qa_results = {
            'name': 'test stack file',
            'description': 'test stack file',
            'environment': 'qa',
            'resources': {
                'test_resource': {
                    'param': 'value',
                    'base_param': 'base_qa_value'
                }
            }
        }

        results = ConfigFactory().load_config_file_from_path(file_path)

        self.assertEqual(expected_test_results, results['test'][0].get_all())
        self.assertEqual(expected_qa_results, results['test'][1].get_all())

    def test_merge_config_recurses_dict(self):
        """Test that dictionaries are merged"""

        file_path = self.__TEST_CONFIG_DIR + '/dictionary'

        expected_resources = {
            'test_resource': {
                'dictionary': {
                    'param': 'value',
                    'base_param': 'base_value'
                }
            }
        }

        results = ConfigFactory().load_config_file_from_path(file_path)
        resources = results['test'][0].get_all()['resources']

        self.assertEqual(expected_resources, resources)

    def test_merge_config_merges_list(self):
        """Test that list values are merged"""

        file_path = self.__TEST_CONFIG_DIR + '/list'

        expected_resources = {
            'test_resource': {
                'list': ['base_value', 'value']
            }
        }

        results = ConfigFactory().load_config_file_from_path(file_path)
        resources = results['test'][0].get_all()['resources']

        self.assertEqual(expected_resources, resources)


    def test_multi_resources_inherit(self):
        """Test that multiple resources inherit base config"""
        file_path = self.__TEST_CONFIG_DIR + '/multi_resource'

        expected_resources = {
            'first__test_resource': {
                'list': ['base_value', 'value']
            },
            'second__test_resource': {
                'list': ['base_value', '2nd value']
            }
        }

        results = ConfigFactory().load_config_file_from_path(file_path)
        resources = results['test'][0].get_all()['resources']

        self.assertEqual(expected_resources, resources)

if __name__ == '__main__':
    unittest.main()

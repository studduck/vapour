"""Unit tests for security_group module"""
import unittest
from unittest.mock import Mock
import template
from troposphere import Template

class TestTemplate(unittest.TestCase):
    """Test the template module"""

    def test_build_builds_template(self):
        """Test that resources are added and file is created"""

        name = 'TestTemplate'
        description = 'template description'
        environment = 'test'
        version = '2010-09-09'

        config = {
            'name': 'test config',
            'description': description,
            'environment': environment,
            'version': version,
            'resources': {
                'resource': {
                    'key': 'value'
                }
            }
        }

        output_directory = '/dir'

        file_handler = Mock()
        resource_handler = Mock()
        config_factory = Mock()
        build_return_value = 'test'
        config_factory.build.side_effect = [build_return_value]
        test_template = Template()

        template.build(
            test_template,
            name,
            config,
            output_directory,
            file_handler,
            resource_handler,
            config_factory
        )

        directory = output_directory + '/' + environment
        file_name = directory + '/' + name + '.template'

        expected_template = Template()
        expected_template.add_version(version)
        expected_template.add_description(description)

        file_handler.write.assert_called_with(
            file_name,
            directory,
            expected_template.to_json()
        )

        config_factory.build.assert_called_with(
            'resource',
            {'key': 'value'},
            environment
        )

        resource_handler.add.assert_called_with(
            'resource',
            environment,
            test_template,
            build_return_value
        )


if __name__ == '__main__':
    unittest.main()
